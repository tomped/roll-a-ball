﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerControler : MonoBehaviour {

	// these variables are public so we can set them in Unity
	public float speed;
	public Text countText;
	public Text winText;

	// only this object needs to worry about these parameters
	private Rigidbody rb;
	private int count;

	void Start () 
	{
		// we get the rigibody of this object
		rb = GetComponent<Rigidbody>();
		// set count to zero
		count = 0;
		// used a method here so we don't have duplicate code
		SetCountText ();
		// by default you have not won the game, so no text is displayedf
		winText.text = "";
	}

	void FixedUpdate()
	{
		// get the horizontal movement
		float moveHorizontal = Input.GetAxis ("Horizontal");
		// get the vertical movement
		float moveVertical = Input.GetAxis ("Vertical");
		// the vector of this obecjt will move on the x and z, but never the y
		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
		// apply that vector force ^
		rb.AddForce (movement * speed);
	}

	/**
	 * Collision? No two colliders can overlap in unity, unity will calulate
	 * a collision. Static colideres and not movies, while dynamic coliders move
	 * Physics can overlap and not cause a collision, you can do this by making the
	 * collider on trigger
	 */
	void OnTriggerEnter(Collider other)
	{
		// tag checking, if we hit that object
		if (other.gameObject.CompareTag("Pick Up"))
		{
			// if we remove game object, then all things with this tag will disapear
			other.gameObject.SetActive(false);
			// when we hit an object 
			count++;
			// read above
			SetCountText ();
		}
	}

	// the method to set the text in the scene
	void SetCountText ()
	{
		// the coins coint
		countText.text = "Count: " + count.ToString ();
		// have we won?

		if (count >= 10)
		{
			winText.text = "You Win!";
		}
	}
}