﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour {
	// public variable so we can drag the player onto the script
	public GameObject player;
	// only this code needs to have this variable
	private Vector3 offset;

	// Use this for initialization
	void Start () {
		// setting the difference
		offset = transform.position - player.transform.position;
	}
	
	// Update is called once per frame
	void LateUpdate () { // to make sure that the player has moved so we update after
		transform.position = player.transform.position + offset;
	}
}